var btnAdicionar = document.querySelector("#adicionar-paciente");
btnAdicionar.addEventListener("click", function(event){
    //desativa as acoes padroes do browser
    event.preventDefault();

    var form = document.querySelector("#form-adiciona");

    //função refatorada a partir de um trecho de codigo,
    //funcao esta que retorna um objeto com os atributos do paciente
    //atributos do formulario.
    var paciente = obtemPctFormulario(form);
    
    var erros = validaPaciente(paciente);

    if(erros.length > 0){
        exibeMensagemErro(erros);
        return;
    }
    //criado função para adicionar pacientes na tabela
    adicionaPacienteNaTabela(paciente);

    form.reset();
    
    //limpando os elementos de mensagens de erro <li> após ter sido preenchido os campos corretamente.
    var mensagensErro = document.querySelector("#mensagens-erro");
    mensagensErro.innerHTML = "";

});

function adicionaPacienteNaTabela(paciente) {
    var pacienteTr = montaTr(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(pacienteTr);
}

function obtemPctFormulario(form){
    //criando as variaveis para receber os valores (values) do formulário
    var paciente = {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }
    return paciente;
}

function montaTr(paciente){
    //criando os elementos DOM do novo paciente - Elemento Pai (Tr) > Filhos (Td)
    var pacienteTr = document.createElement('tr');
    //adicionando a classe aos novos <tr>
    pacienteTr.classList.add("paciente");

    //definindo a hierarquia que o DOM necessita
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;
}

function montaTd(dado, classe){
    //criando os elementos DOM do novo paciente - Elemento Pai (Tr) > Filhos (Td)
    //refatorado nessa funcao
    var td = document.createElement("td");

    td.textContent = dado;
    td.classList.add(classe);

    return td;
}

function validaPaciente(paciente){

    var erros = [];

    if(paciente.nome.length == 0){
        erros.push("Preencha o campo nome");
    }

    if(paciente.peso.length == 0){
        erros.push("Preencha o campo peso");
    }
    
    if(paciente.altura.length == 0){
        erros.push("Preencha o campo altura");
    }

    if(paciente.gordura.length == 0){
        erros.push("Preencha o campo gordura");
    }

    if(!validaPeso(paciente.peso)){
       erros.push("Peso inválido");
    }

    if(!validaAltura(paciente.altura)){
        erros.push("Altura inválida!");
    }
    return erros;
}

function exibeMensagemErro(erros) {
    var ul = document.querySelector("#mensagens-erro");

    ul.innerHTML = "";

    erros.forEach(function(erro){
        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);
    });
}