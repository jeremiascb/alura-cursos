var pacientes = document.querySelector("#tabela-pacientes");

pacientes.addEventListener("dblclick",function(event){
    //adicionando classe para efeito de opacidade
    event.target.parentNode.classList.add("fadeOut");

    //funcao que faz javascript aguardar para executar funcao seguinte
    setTimeout(function(){
        //removendo o elemento pai <tr> do elemento clicado <td>
        event.target.parentNode.remove();
    },500);
    
});