var campoFiltro = document.querySelector("#campo-filtro");

campoFiltro.addEventListener("input",function(){
    var pacientes = document.querySelectorAll(".paciente");

    if(this.value.length > 0){
        for (var i = 0; i < pacientes.length; i++){
            var paciente = pacientes[i];
            var tdNome = paciente.querySelector(".info-nome");
            var nomePaciente = tdNome.textContent;

            //adicionando busca por expressao regular "i"-> busca insensitive
            var expressao = new RegExp(this.value, "i");
    
            if(expressao.test(nomePaciente)){
                paciente.classList.remove("invisivel");
            }else{
                paciente.classList.add("invisivel");
            }
        }
    }else{
        for(var i = 0; i < pacientes.length; i++){
            var paciente = pacientes[i];
            paciente.classList.remove("invisivel");
        }
    }
});