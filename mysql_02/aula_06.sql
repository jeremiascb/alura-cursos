select a.nome, avg(n1.nota) as media, (select avg(n2.nota) from nota n2) as media_geral,
avg(n1.nota) - (select avg(n2.nota) from nota n2
join resposta r2 on n2.resposta_id = r2.id where r2.aluno_id <> a.id) as diferenca
from 
nota n1
join resposta r on r.id = n1.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join aluno a on a.id = r.aluno_id
where 
a.id in (select aluno_id from matricula where data > '2017-11-01' - interval 2 month)
group by a.nome