create table usuarios(
id integer auto_increment not null,
nome varchar(50) not null,
idade varchar(3) not null,
data_nascimento date,
primary key(id));

create table emails(
id integer auto_increment not null,
usuario_id integer not null,
email varchar(50) not null,
primary key(id),
foreign key(usuario_id) references usuarios(id));

insert into usuarios(nome, idade, data_nascimento) values ('Cristiana da Costa Borges Rodrigues','26','19921002');

select * from usuarios;

insert into emails(usuario_id,email) values (2,'uzielcb@gmail.com');
insert into emails(usuario_id,email) values (3,'kriz.5@hotmail.com');

select * from emails;

select nome, e.email from usuarios join emails e on usuarios.id = e.usuario_id;

